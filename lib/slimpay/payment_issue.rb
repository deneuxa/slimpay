module Slimpay
  class PaymentIssue < Resource

    def search(params)
      url = "#{@endpoint}/#{@resource_name}"
      JSON.load(HTTParty.get(url, headers: options, query: params))
    end

    def acknowledge(id)
      url = "#{@endpoint}/#{@resource_name}/#{id}/ack"
      body_options = {
        creditor: {reference: @creditor_reference}
      }
      JSON.load(HTTParty.post(url, body: body_options.to_json, headers: options))
    end
  end
end
